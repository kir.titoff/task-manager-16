package ru.t1.ktitov.tm.service;

import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.service.ITaskService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return add(new Task(name));
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public void updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        task.setStatus(status);
    }

    @Override
    public void changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (index >= taskRepository.getSize()) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        task.setStatus(status);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(id);
    }

    @Override
    public void removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        taskRepository.removeByIndex(index);
    }

}
